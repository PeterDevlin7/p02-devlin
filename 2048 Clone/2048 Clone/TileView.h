//
//  TileView.h
//  2048 Clone
//
//  Created by Peter Devlin on 2/8/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TileView : UIView

@property (nonatomic, strong) UILabel *label;
@property (nonatomic) int value;

-(void)updateLabel;
@end

