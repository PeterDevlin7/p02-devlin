//
//  GameView.h
//  2048 Clone
//
//  Created by Peter Devlin on 2/8/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TileView.h"

@interface GameView : UIView
@property (nonatomic, strong) NSArray *directions;
@property (nonatomic, strong) NSMutableArray *tiles;

@property (nonatomic, strong) IBOutlet UILabel *scoreLabel;
@property (nonatomic, strong) IBOutlet UIViewController *vc;

@end
