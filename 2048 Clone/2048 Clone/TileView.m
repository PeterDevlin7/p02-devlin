//
//  TileView.m
//  2048 Clone
//
//  Created by Peter Devlin on 2/8/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import "TileView.h"

@implementation TileView
@synthesize label;
@synthesize value;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:label];
        [label setText:@""];
        [label setTextAlignment:NSTextAlignmentCenter];
        [self setBackgroundColor:[UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0]];
    }
    return self;
}

-(void)updateLabel
{
    if (value == 0)
        [label setText:@""];
    else
        [label setText:[NSString stringWithFormat:@"%d", value]];
    
    float base = 100.0/255.0;
    float dred = (7.0*log2f(value+2.0))/255.0;
    float dgreen = (8.0*log2f(value+2.0))/255.0;
    float dblue = (10.0*log2f(value+2.0))/255.0;
    float red = base + dred;
    float green = base + dgreen;
    float blue = base + dblue;
    if(red > 1.0) red = 1.0;
    if(green > 1.0) green = 1.0;
    if(blue > 1.0) blue = 1.0;
    
    [self setBackgroundColor: [UIColor colorWithRed:red green:green blue:blue alpha:1.0]];
}

@end
