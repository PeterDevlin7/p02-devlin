//
//  ViewController.m
//  2048 Clone
//
//  Created by Peter Devlin on 1/24/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.startButton.layer.cornerRadius = 7;
    self.resetButton.layer.cornerRadius = 7;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
