//
//  GameView.m
//  2048 Clone
//
//  Created by Peter Devlin on 2/8/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize directions;
@synthesize tiles;
@synthesize scoreLabel;
@synthesize vc;
int score = 0;
//NSNotification *gameOverPopup;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(int)value:(int)direction row:(int)row col:(int)col
{
    NSArray *a = [directions objectAtIndex:direction];
    a = [a objectAtIndex:row];
    NSLog(@"Array A has %d elements", (int)[a count]);
    
    for (NSNumber *b in a)
    {
        NSLog(@"  Contains %@", b);
        
    }
    NSNumber *b = [a objectAtIndex:col];
    
    return (int)[b integerValue];
}

// To make life easier, I'm creating arrays with the indices
// for each of the four possible compression directions.
//
// There's north, south, east, west.  The tile spots are
// numbered 0  1  2  3
//          4  5  6  7
//          8  9 10 11
//         12 13 14 15

-(void)generateTiles
{
    for (int i = 0; i < 3; ++i)
    {
        TileView *tv = [self findEmptyTile];
        if (tv)
        {
            [tv setValue: [self determineValue]];
            [tv updateLabel];
        }
    }
}

-(int)determineValue
{
    int randomNumber = arc4random_uniform(8);
    if (randomNumber < 7) {
        return 2;
    }
    else {
        return 4;
    }
}

-(TileView *)findEmptyTile
{
    int i = rand() % 16;
    for (int j = 0; j < 15; ++j)
    {
        TileView *tv = [tiles objectAtIndex:(i + j)%16];
        if ([tv value] == 0)
            return tv;
    }
    
    return nil;
}

-(TileView *)findEmptyTile:(int)direction
{
    int options[4];
    switch (direction) {
        case 0:
            options[0] = 12; options[1] = 13; options[2] = 14; options[3] = 15;
            break;
        case 1:
            options[0] = 0; options[1] = 1; options[2] = 2; options[3] = 3;
            break;
        case 2:
            options[0] = 0; options[1] = 4; options[2] = 8; options[3] = 12;
            break;
        case 3:
            options[0] = 3; options[1] = 7; options[2] = 11; options[3] = 15;
            break;
        default:
            break;
    }
    int i = rand() % 4;
    TileView *tv;
    for (int j = 0; j < 4; ++j) {
        TileView *tv = [tiles objectAtIndex:options[i%4]];
        if (tv.value == 0) {
            return tv;
        }
        i++;
    }
    return tv;
}


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        tiles = [[NSMutableArray alloc] init];
        CGRect bounds = [self bounds];
        float w = bounds.size.width/4;
        float h = bounds.size.height/4;
        for (int r = 0; r < 4; ++r)
        {
            for (int c = 0; c < 4; ++c)
            {
                TileView *tv = [[TileView alloc] initWithFrame:CGRectMake(c*w + 4, r*h + 4, w - 8, h - 8)];
                [tiles addObject:tv];
                [tv setValue:0];
                [tv updateLabel];
                [self addSubview:tv];
            }
        }
        
        NSArray *north = @[@[@( 0), @( 4), @( 8), @(12)],
                           @[@( 1), @( 5), @( 9), @(13)],
                           @[@( 2), @( 6), @(10), @(14)],
                           @[@( 3), @( 7), @(11), @(15)]];
        NSArray *south = @[@[@(12), @( 8), @( 4), @( 0)],
                           @[@(13), @( 9), @( 5), @( 1)],
                           @[@(14), @(10), @( 6), @( 2)],
                           @[@(15), @(11), @( 7), @( 3)]];
        NSArray *east  = @[@[@( 3), @( 2), @( 1), @( 0)],
                           @[@( 7), @( 6), @( 5), @( 4)],
                           @[@(11), @(10), @( 9), @( 8)],
                           @[@(15), @(14), @(13), @(12)]];
        NSArray *west  = @[@[@( 0), @( 1), @( 2), @( 3)],
                           @[@( 4), @( 5), @( 6), @( 7)],
                           @[@( 8), @( 9), @(10), @(11)],
                           @[@(12), @(13), @(14), @(15)]];
        
        
        directions = @[north, south, east, west];
        [self generateTiles];
    }
    return self;
}

-(IBAction)resetGame:(id)sender
{
    for (int i = 0; i < 16; ++i)
    {
        TileView *tv = [tiles objectAtIndex:i];
        [tv setValue:0];
        [tv updateLabel];
    }
    [self generateTiles];
    [scoreLabel setText:@"Score: 0"];
}

// Compression in a given direction.  The
// button that triggers the call to this method should
// have a Tag set of 0, 1, 2, or 3, for each of the
// different directions.
//
// We use the tile indexing from the directions array,
// and then go through each of the four rows, inserting
// the numbers as needed.
//
-(IBAction)compress:(UISwipeGestureRecognizer*)sender
{
    int oldValues[4];
    int newValues[4];
    int merged[4];
    int direction;
    BOOL validMove = FALSE;
    
    switch (sender.direction) {
        case UISwipeGestureRecognizerDirectionUp:
            direction = 0;
            break;
        case UISwipeGestureRecognizerDirectionDown:
            direction = 1;
            break;
        case UISwipeGestureRecognizerDirectionRight:
            direction = 2;
            break;
        case UISwipeGestureRecognizerDirectionLeft:
            direction = 3;
            break;
        default:
            direction = 0;
            break;
    }
    
    NSArray *compression = [directions objectAtIndex:direction];
    for (int row = 0; row < 4; ++row)
    {
        NSLog(@"Extract row %d", row);
        // Get the indices of the spots we want to compress
        NSArray *thisRow = [compression objectAtIndex:row];
        
        for (int i = 0; i < 4; ++i)
        {
            newValues[i] = 0;
            merged[i] = 0;
        }
        int index = 0;
        
        for (int i = 0; i < 4; ++i)
        {
            NSNumber *n = [thisRow objectAtIndex:i];
            TileView *tv = [tiles objectAtIndex:[n integerValue]];
            NSLog(@"Tile %d has value %d", i, [tv value]);
            oldValues[i] = [tv value];
            if ([tv value] > 0)
            {
                if (index == 0)
                {
                    NSLog(@"Insert %d", [tv value]);
                    newValues[index++] = [tv value];
                }
                else
                {
                    if ((newValues[index - 1] == [tv value]) && (!merged[index - 1]))
                    {
                        NSLog(@"Merge to %d", [tv value] * 2);
                        newValues[index - 1] = newValues[index - 1] * 2;
                        merged[index - 1] = 1;
                        score += newValues[index - 1];
                        [scoreLabel setText:[NSString stringWithFormat:@"Score: %d", score]];
                    }
                    else
                    {
                        NSLog(@"Insert %d", [tv value]);
                        newValues[index++] = [tv value];
                    }
                }
            }
        }
        for (int i = 0; i < 4; ++i) {
            if (oldValues[i] != newValues[i]) {
                validMove = TRUE;
            }
        }
        
        // Fix up the labels!
        for (int i = 0; i < 4; ++i)
        {
            NSNumber *n = [thisRow objectAtIndex:i];
            TileView *tv = [tiles objectAtIndex:[n integerValue]];
            [tv setValue:newValues[i]];
            [tv updateLabel];
        }
    }
    
    if (validMove) {
        
        TileView *tv = [self findEmptyTile:direction];
        [tv setValue: [self determineValue]];
        [tv updateLabel];
        [self determineGameOver];
    }
    
}

-(void)determineGameOver {
    //NSLog(@"Testing");
    for(int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j) {
            int index = (4*i)+j;
            NSLog(@"Index: %d", index);
            int upval = -1;
            int downval = -1;
            int leftval = -1;
            int rightval = -1;
            TileView *tv = [tiles objectAtIndex: index];
            if(tv.value == 0) return;
            TileView *tvup = (index-4 >= 0) ? tiles[index-4] : NULL;
            if(tvup != NULL) upval = tvup.value;
            TileView *tvdown = (index+4 <= 15) ? tiles[index+4] : NULL;
            if(tvdown != NULL) downval = tvdown.value;
            TileView *tvleft = ((index-1 >= 0) && ((index-1)%4 < index%4)) ? tiles[index-1] : NULL;
            if(tvleft != NULL) leftval = tvleft.value;
            TileView *tvright = ((index+1 <= 15) && ((index+1)%4 > index%4)) ? tiles[index+1] : NULL;
            if(tvright != NULL) rightval = tvright.value;
            if ((upval == tv.value) ||
                (downval == tv.value) ||
                (leftval == tv.value) ||
                (rightval == tv.value)) {
                return;
            }
        }
    }
    //Game Over
    NSLog(@"Game Over!");
    NSString *comment = @"How did you even manage that?";
    if (score > 250) {
        comment = @"There's always room for improvement...";
    }
    if (score > 500) {
        comment = @"Better luck next time.";
    }
    if (score > 1000) {
        comment = @"Getting closer.";
    }
    if (score > 1250) {
        comment = @"Try harder.";
    }
    if (score > 1500) {
        comment = @"Not bad, but not good.";
    }
    if (score > 2000) {
        comment = @"Practice makes perfect";
    }
    if (score > 4000) {
        comment = @"Practice makes perfect.";
    }
    if (score > 8000) {
        comment = @"Pretty close bud.";
    }
    if (score > 15000) {
        comment = @"Don't give up now! Try again.";
    }
    if (score > 25000) {
        comment = @"Noice m8, ya did it.";
    }
    NSString *scoreString = [NSString stringWithFormat:@"You got a score of %d.\n%@", score,comment];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Game Over"
                                                                   message:scoreString
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [vc presentViewController:alert animated:YES completion:nil];
    
}

@end
